.POSIX:

#
# Paths:
#
BINDIR= $(PREFIX)/bin

#
# Commands:
#
CXX=            /usr/bin/c++
CXXFLAGS=       -O2 -isystem include -std=c++11 -pedantic -Wall -Wextra -Wconversion
LDFLAGS=        -stdlib=libc++
INSTALL=        /usr/bin/install
RM=             /bin/rm -rf
INSTALL_PROGRAM=$(INSTALL) -s -m 0755
INSTALL_SCRIPT= $(INSTALL)    -m 0755

#
# Files:
#
TABULARIZE_OBJS=    tabularize.o
TABULARIZE_PRODUCTS=tabularize

DING_OBJS=          ding.o
DING_PRODUCTS=      ding
DING_LIBS=          -pthread

OBJS=               $(TABULARIZE_OBJS)     $(DING_OBJS)
PRODUCTS=           $(TABULARIZE_PRODUCTS) $(DING_PRODUCTS)

#
# Double-suffix rules:
#
.SUFFIXES: .cc .o

.cc.o:
	$(CXX) $(CXXFLAGS) -c $<

#
# Targets:
#
.PHONY: all                \
        check-install      \
        install-git-ls     \
        install-tabularize \
        install            \
        clean


all: $(PRODUCTS)
	@:

check-install:
	@test -n "$(PREFIX)" || (echo "Installation PREFIX is not set."; false)
	@test -d "$(BINDIR)" || (echo "Directory $(BINDIR) does not exist."; false)

install-git-ls: check-install git-ls.sh
	$(INSTALL_SCRIPT) git-ls.sh $(BINDIR)/git-ls

tabularize: $(TABULARIZE_OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(TABULARIZE_OBJS)

ding: $(DING_OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(DING_OBJS) $(DING_LIBS)

install-tabularize: check-install tabularize
	$(INSTALL_PROGRAM) tabularize $(BINDIR)

install: install-tabularize install-git-ls
	@:

clean:
	$(RM) $(OBJS) $(PRODUCTS)
